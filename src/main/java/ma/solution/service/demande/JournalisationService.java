package ma.solution.service.demande;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ma.solution.dao.demande.IJournalisationDao;
import ma.solution.model.demande.Journalisation;
import ma.solution.tools.Constante;


@Service
@EnableJms
public class JournalisationService implements IJournalisationService {
	
	@Autowired
	private IJournalisationDao journalisationDao;
	
	private static final Logger logger = LoggerFactory.getLogger(JournalisationService.class);
	
	@JmsListener(destination = Constante.message_queue)
	private void journalisation(String action) {
		Journalisation journalisation = new Journalisation();
		try {
			journalisation.setAction(action);
			journalisation.setDateAction(new Date());
			journalisationDao.saveAndFlush(journalisation);
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Erreur au niveau de la journalisation : ", e.getMessage());
		}
	}
}
