package ma.solution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"ma.solution"})
public class CIMRServiceJournalisationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CIMRServiceJournalisationApplication.class, args);
	}

}
