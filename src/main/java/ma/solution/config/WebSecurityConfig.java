//package ma.solution.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
// 
//    @Autowired
//    private UserDetailsService userDetailsService;
//    
//    @Bean
//    public BCryptPasswordEncoder bCryptPasswordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
// 
////    @Autowired
////    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
////    }
//    
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//        		.csrf().disable()	
//        		.authorizeRequests()
//        				.antMatchers("/login","/index","/").permitAll()
////        				.antMatchers("/login","/index","/","/initAddMarhaba","/initSuiviMarhaba","/isExisteDemande","/isExisteRib","/isRibValide","/initEditBeneficiaire","/editModePaiement").permitAll()
//        				.antMatchers("/js/**","/css/**", "/assets/**","/login").permitAll()
////        				.anyRequest().authenticated()
//        				.anyRequest().permitAll()
//        				.and()
//                .formLogin()
//                		.loginPage("/login")
//                		.loginProcessingUrl("/login")
//                		.defaultSuccessUrl("/indexMarhaba")
//                		.failureUrl("/login")
//                		.permitAll()
//                		.and()
//                .logout()
//                	.permitAll();
//        		
//        http.headers().frameOptions().disable();
//        
//        http.sessionManagement().maximumSessions(1).expiredUrl("/login");
//    }
////    
////    @Bean(name = "multipartResolver")
////    public MultipartResolver multipartResolver() 
////    {
////       return new StandardServletMultipartResolver();
////    }
//}
