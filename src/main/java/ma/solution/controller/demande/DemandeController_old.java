//package ma.solution.controller.demande;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.servlet.http.HttpSession;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import ma.solution.service.beans.demande.DemandeBean;
//import ma.solution.service.beans.demande.ObjetBean;
//import ma.solution.service.beans.demande.VehiculeBean;
//import ma.solution.service.demande.IDemandeService;
//import ma.solution.service.parametrage.IParametrageService;
//import ma.solution.tools.Constante;
//import ma.solution.tools.DateUtils;
//
//@Controller
//public class DemandeController {
//
//	@Autowired
//	IParametrageService parametrageService;
//	
//	@Autowired
//	IDemandeService demandeService;
//	
//	@Autowired
//	private HttpSession session;
//	
//	private static final Logger logger = LoggerFactory.getLogger(DemandeController.class);
// 
//	@RequestMapping("/initAddDemande")
//	public String initAddDemande(DemandeBean demandeBean, Model model) {
//		List<ObjetBean> banqueBeans = new ArrayList<ObjetBean>();
//		List<ObjetBean> activiteBeans = new ArrayList<ObjetBean>();
//		List<ObjetBean> regionBeans = new ArrayList<ObjetBean>();
//		List<ObjetBean> villeBeans = new ArrayList<ObjetBean>();
//		Double montantIndemnisation = 0D;
//		try {
//			
//			activiteBeans = parametrageService.getAllActivite();
//			regionBeans = parametrageService.getAllRegion();
//			banqueBeans = parametrageService.getAllBanque();
//			villeBeans = parametrageService.getAllVille();
//			demandeBean = demandeService.getDemandeByTypeActivite(demandeBean);
//			if(demandeBean != null && demandeBean.getVehiculeBeans() != null && !demandeBean.getVehiculeBeans().isEmpty()) {
//				for(VehiculeBean vehiculeBean : demandeBean.getVehiculeBeans()) {
//					if(Constante.etat_vehicule_valider.equals(vehiculeBean.getEtat())) {
//						montantIndemnisation=montantIndemnisation+vehiculeBean.getMontant();
//					}
//				} 
//			}
//			session.setAttribute("montantIndemnisation", montantIndemnisation);
//		}catch (Exception e) {
//			logger.error("Controller : Erreur lors de chargement de la page de depot des demandes  : "+ e.getMessage());
//		}
//		model.addAttribute("villeBeans",villeBeans);
//		model.addAttribute("banqueBeans",banqueBeans);
//		model.addAttribute("activiteBeans",activiteBeans);
//		model.addAttribute("regionBeans",regionBeans);
//		model.addAttribute("demandeBean", demandeBean);
//		return "addDemande";
//	}
//	
//	
//	
//	@RequestMapping("/initSuiviDemande")
//	public String initSuiviDemande(DemandeBean demandeBean,Model model) {
//		try {
//			demandeBean.setFlagRecherche(false);
//		}catch (Exception e) {
//			logger.error("Controller : Erreur lors de rechercher sur une demande :suiviDemande  : "+ e.getMessage());
//		}
//		model.addAttribute("demandeBean", demandeBean);
//		return "suiviDemande";
//	}
//	
//	@RequestMapping("/suiviDemande")
//	public String suiviDemande(DemandeBean demandeBean,Model model) {
//		List<DemandeBean> demandeBeans = new ArrayList<DemandeBean>();
//		try {
//			demandeBean.setFlagRecherche(true);
//		}catch (Exception e) {
//			logger.error("Ereur : "+ e.getMessage());
//		}
//		model.addAttribute("demandeBean", demandeBean);
//		model.addAttribute("demandeBeans", demandeBeans);
//		return "suiviDemande";
//	}
//	
////	@RequestMapping("/getVehiculeByDemande")
////	public String getVehiculeByDemande(DemandeBean demandeBean,Model model) {
////		List<DemandeBean> demandeBeans = new ArrayList<DemandeBean>();
////		try {
////			List<VehiculeBean> vehiculeBeans = demandeService.getVehiculesByRcAndPatente(demandeBean);
////			if(vehiculeBeans != null) {
////				demandeBean.setVehiculeBeans(vehiculeBeans);
////			}
////		}catch (Exception e) {
////			logger.error("Ereur : "+ e.getMessage());
////		}
////		model.addAttribute("demandeBean", demandeBean);
////		model.addAttribute("demandeBeans", demandeBeans);
////		return "addDemande";
////	}
//	
//	@RequestMapping("/validerVehicule")
//	@ResponseBody
//	public VehiculeBean validerVehicule(Long vehicule, String numeroChassis) {
//		VehiculeBean vehiculeBean = null;
//		Double montant = 0D;
//		try {
//			vehiculeBean = demandeService.validerVehicule(vehicule, numeroChassis);
//			if(session.getAttribute("montantIndemnisation") != null) {
//				montant = (Double) session.getAttribute("montantIndemnisation") ;
//			}
//			session.setAttribute("montantIndemnisation", montant+vehiculeBean.getMontant());
//			vehiculeBean.setMontant(montant+vehiculeBean.getMontant());
//		} catch (Exception e) {
//			logger.error("erreur 3");
//		}
//		return vehiculeBean;
//	}
//	
//	@RequestMapping("/validerDemande")
//	@ResponseBody
//	public String validerDemande(Long demande) {
//		String code="";
//		try {
//			DemandeBean demandeBean = demandeService.accepterDemande(demande);
//			code ="A"+demandeBean.getId()+"YZT"+DateUtils.dateToStringAutre(new Date());
//		} catch (Exception e) {
//			logger.error("erreur 3");
//		}
//		return code;
//	}
//	
//	
//	@RequestMapping("/isRibValide")
//	@ResponseBody
//	public Boolean isRibValide(String rib) {
//		Boolean result = false;
//		String ribCleFinal = "";
//		try {
//			rib = rib.replaceAll("-", "");
//			String ribCleInitial = rib.substring(22,24);
//			String ribBis = rib.substring(0, 22)+"00";
//			String rib1=ribBis.substring(0, 10);
//
//			int rest = (int) (Double.parseDouble(rib1)%97);
//
//			String rib2 = rest  + ribBis.substring(10,20);
//
//			rest = (int) (Double.parseDouble(rib2)%97);
//
//			rib2 = rest  + ribBis.substring(20, 24);
//
//			rest = (int)(Double.parseDouble(rib2)%97);
//
//			String cleRib = String.valueOf(97-rest);
//			if(cleRib.length() == 1) {
//				ribCleFinal = "0"+cleRib;
//			} else {
//				ribCleFinal = cleRib;
//			}
//			if(ribCleInitial.equals(ribCleFinal)) {
//				result = true;
//			}
//		} catch (Exception e) {
//			logger.error("Erreur ---> methode : isRibValide  ------->Message : "+e.getMessage());
//		}
//		return result;
//	}
//}