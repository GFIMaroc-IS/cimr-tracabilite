package ma.solution.dao.demande;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.solution.model.demande.Journalisation;

@Repository
public interface IJournalisationDao extends JpaRepository<Journalisation, Long> {

}
